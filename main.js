const { app, BrowserWindow, ipcMain, Menu, MenuItem} = require('electron');
const fs = require('fs');
const path = require('path');
const Nodehun = require('nodehun');
const dictionaryPath = path.join( __dirname,'dictionaries');

let language = "de"
let affix = null;
let dictionary = null;

if (language === "en") {
    affix       = fs.readFileSync(path.join(dictionaryPath, 'en_US.aff'))
    dictionary  = fs.readFileSync(path.join(dictionaryPath, 'en_US.dic'))
}
else if (language === "de"){
    affix       = fs.readFileSync(path.join(dictionaryPath, 'de_DE_frami.aff'))
    dictionary  = fs.readFileSync(path.join(dictionaryPath, 'de_DE_frami.dic'))
}
else if (language === "it"){
    affix       = fs.readFileSync(path.join(dictionaryPath, 'it_IT.aff'))
    dictionary  = fs.readFileSync(path.join(dictionaryPath, 'it_IT.dic'))
}
else if (language === "fr"){
    affix       = fs.readFileSync(path.join(dictionaryPath, 'fr.aff'))
    dictionary  = fs.readFileSync(path.join(dictionaryPath, 'fr.dic'))
}
else if (language === "es"){
    affix       = fs.readFileSync(path.join(dictionaryPath, 'es_ES.aff'))
    dictionary  = fs.readFileSync(path.join(dictionaryPath, 'es_ES.dic'))
}


const nodehun  = new Nodehun(affix, dictionary)

let mainWindow = null;
function createWindow() {
    mainWindow = new BrowserWindow({
        title: "My Electron App",
        width: 1200,
        height: 600,
        webPreferences: {
            preload: `${__dirname}/preload.js`,
            spellcheck: false
        }
    });
    mainWindow.loadFile('index.html');
    mainWindow.removeMenu();
    mainWindow.webContents.openDevTools();
}
app.whenReady().then( ()=> {
    createWindow()
});
app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') app.quit();
});

 ipcMain.on('checkword', async (event, selectedWord) => {
    console.log(`Received selected text: ${selectedWord}`);
    const suggestions = await nodehun.suggest(selectedWord)
    console.log(suggestions)
    event.returnValue = {  suggestions : suggestions }   
});




ipcMain.on('checktext', async (event, selectedText) => {
    //console.log(`Received selected text: ${selectedText}`);
    const words = selectedText.split(/[^a-zA-ZäöüÄÖÜ]+/); // Include special characters
    const misspelledWords = [];
    for (const word of words) {
        const correct = await nodehun.spell(word);
        if (!correct) {
            misspelledWords.push(word);
            console.log(word)
        }
    }
    event.returnValue = { misspelledWords };


});

