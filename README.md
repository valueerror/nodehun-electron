# NodeHun Electron


This Project is a Demoproject for NodeHun and Electron.
It uses NodeHun to check misspelled words and plain javascript to highlight and correct them in the frontend.

## install necessary modules 

```npm i```

## run nodehun-electron example 

```npm run start```


![screenshot](/screenshot.png)


