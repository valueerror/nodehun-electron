const { contextBridge, ipcRenderer } = require('electron');

// Expose ipcRenderer to the window object for the renderer process
contextBridge.exposeInMainWorld('ipcRenderer', ipcRenderer);